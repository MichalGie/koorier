FROM python:3.6

ENV PYTHONUNBUFFERED 1

COPY . /code/
WORKDIR /code/
RUN pip3 install pipenv
RUN pip3 install -r /code/req.txt

EXPOSE 8000