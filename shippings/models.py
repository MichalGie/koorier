from django import forms
from base64 import b64decode
from django.core import validators
from django.core.files.base import ContentFile
from django.db import models
from django.db.models.signals import pre_save, post_save, m2m_changed
from django.dispatch import receiver
from django.contrib.auth.models import User,  Group
from django.utils.safestring import mark_safe
from django.utils.timezone import datetime
from shippings.utils import generate_shiping_code
from geopy.geocoders import Nominatim
from shippings.utils import string_generator
from shippings.validators import phone_validator

def get_user_full_name(self):
	if self.get_full_name() is "":
		return self.username
	return self.get_full_name()
User.add_to_class("__str__", get_user_full_name)


class Shipping(models.Model):

	def __str__(self):
		return self.recipient_city + " (" + self.code + ")"

	class Meta:
		verbose_name = 'Przesyłka'
		verbose_name_plural = '1. Przesyłki'

	code				= models.CharField(max_length=32, blank=True, null=False, verbose_name="Kod przesyłki")
	width 				= models.FloatField(blank=False, null=False, verbose_name="Szerokość przesyłki(cm)")
	height				= models.FloatField(blank=False, null=False, verbose_name="Wysokość przesyłki(cm")
	weight				= models.FloatField(blank=False, null=False, verbose_name="Waga przesyłki(kg)")
	depth 				= models.FloatField(blank=False, null=False, verbose_name="Głębokość przesyłki(cm)")
	sender_fullname 	= models.CharField(max_length=64, blank=False, null=False, verbose_name="Nadawca", validators=[validators.MinLengthValidator(5, message="Field sender fullname require to have at least 5 characters!"),])
	sender_phone	 	= models.CharField(max_length=64, blank=False, null=False, verbose_name="Numer tel. nadwacy", validators=[phone_validator,])
	sender_email	 	= models.CharField(max_length=64, blank=False, null=False, verbose_name="Email nadawcy", validators=[validators.MinLengthValidator(5, message="Field sender sender email require to have at least 5 characters!"),])
	sender_adress 		= models.CharField(max_length=64, blank=False, null=False, verbose_name="Adres nadawcy", validators=[validators.MinLengthValidator(5, message="Field sender sender adress require to have at least 5 characters!"),])
	sender_city 		= models.CharField(max_length=64, blank=False, null=False, verbose_name="Miasto nadawcy", validators=[validators.MinLengthValidator(5, message="Field sender sender city require to have at least 5 characters!"),])
	sender_post_code 	= models.CharField(max_length=6, blank=False, null=False, verbose_name="Kod pocztowy nadawcy", validators=[validators.MinLengthValidator(5, message="Field sender sender post code require to have at least 5 characters!"),])
	recipient_fullname	= models.CharField(max_length=64, blank=False, null=False, verbose_name="Odbiorca", validators=[validators.MinLengthValidator(5, message="Field sender recipient fullname require to have at least 5 characters!"),])
	recipient_phone	 	= models.CharField(max_length=64, blank=False, null=False, verbose_name="Numer tel. odbiorcy", validators=[phone_validator,])
	recipient_email	 	= models.CharField(max_length=64, blank=False, null=False, verbose_name="Emial odbiorcy", validators=[validators.MinLengthValidator(5, message="Field sender recipient email require to have at least 5 characters!"),])
	recipient_adress 	= models.CharField(max_length=64, blank=False, null=False, verbose_name="Adres odbiorcy", validators=[validators.MinLengthValidator(5, message="Field sender recipient adress require to have at least 5 characters!"),])
	recipient_city 		= models.CharField(max_length=64, blank=False, null=False, verbose_name="Miasto odbiorcy", validators=[validators.MinLengthValidator(5, message="Field sender recipient city require to have at least 5 characters!"),])
	recipient_post_code = models.CharField(max_length=6, blank=False, null=False, verbose_name="Kod pocztowy odbiorcy", validators=[validators.MinLengthValidator(5, message="Field sender recipient post code require to have at least 5 characters!"),])
	recipient_lon		= models.FloatField(blank=True, null=True, verbose_name="Koord. obiorcy (lon)")
	recipient_lat		= models.FloatField(blank=True, null=True, verbose_name="Koord. odbiorcy (lat)")
	delivered			= models.BooleanField(default=False, blank=False, null=False, verbose_name="Dostarczona")
	updated 			= models.DateTimeField(auto_now=True, verbose_name="Zmieniono")
	created 			= models.DateTimeField(auto_now_add=True, verbose_name="Utworzono")

	def get_updated(self):
		return self.updated.strftime("%d.%m.%Y %H:%M")
	get_updated.short_description = "Zmieniono"

	def get_created(self):
		return self.created.strftime("%d.%m.%Y %H:%M")
	get_created.short_description = "Utworzono"


@receiver(pre_save, sender=Shipping)
def pre_save_shipping_callback(sender, instance, *args, **kwargs):
	if not instance.code:
		try:
			geolocator = Nominatim()
			location = geolocator.geocode(instance.recipient_adress + " " + instance.recipient_city + " " + instance.recipient_post_code)
			instance.recipient_lon = location.latitude
			instance.recipient_lat = location.longitude
			instance.code = generate_shiping_code(Shipping)
		except:
			pass
			#log error

@receiver(post_save, sender=Shipping)
def post_save_shipping_callback(sender, instance, *args, **kwargs):
	if instance.shipping_delivery_status.count() is 0:
		ShippingDeliveryStatus.initialize_shipping_status(instance)


class ShippingStatus(models.Model):
	
	def __str__(self):
		return self.code + " (" + self.name + ")"

	class Meta:
		verbose_name = 'Zdefiniowany Status przesyłki'
		verbose_name_plural = '7. Zdefiniowane Statusy Przesyłek'

	code 		= models.CharField(max_length=5, blank=False, null=False, verbose_name="Kod przesyłki")
	name 		= models.CharField(max_length=64, blank=False, null=False, verbose_name="Nazwa statusu")
	description = models.CharField(max_length=129, blank=False, null=False, verbose_name="Opis statusu")
	auth_group 	= models.ForeignKey(Group, on_delete=models.CASCADE, verbose_name="Upoważniona grupa")

	def get_initialize_status():
		return ShippingStatus.objects.get(code="n01")

	def get_in_delivery_status():
		return ShippingStatus.objects.get(code="l03")

	def get_code_with_name(self):
		return self.name + "(" + self.code + ")"


class CourierDeliveryLog(models.Model):

	def __str__(self):
		return str(self.courier)

	class Meta:
		verbose_name = 'Koordynaty Kurierów'
		verbose_name_plural = '5. Koordynaty Kurierów'

	courier 	= models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="Kurier")
	lon 		= models.FloatField(blank=True, null=True, verbose_name="Koord. kuriera (lon)")
	lat			= models.FloatField(blank=True, null=True, verbose_name="Koord. kuriera (lat)")
	created 	= models.DateTimeField(auto_now_add=True, verbose_name="Utworzono")

	def get_created(self):
		return self.created.strftime("%d.%m.%Y %H:%M")
	get_created.short_description = "Utworzono"

	def get_courier_full_name(self):
		return str(self.courier)


class ShippingDelivery(models.Model):

	def __str__(self):
		return str(self.courier) + " (" + self.get_created() + ")"

	class Meta:
		verbose_name = 'Dostawa kurierska'
		verbose_name_plural = '4. Dostawy kurierskie'

	shipping 	= models.ManyToManyField(Shipping, related_name='shipping_delivery', blank=False, verbose_name="Przesyłki")
	courier 	= models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="Kurier")
	created 	= models.DateTimeField(auto_now_add=True, verbose_name="Utworzono")
	
	def get_created(self):
		return self.created.strftime("%d.%m.%Y %H:%M")
	get_created.short_description = "Utworzono"


@receiver(m2m_changed, sender=ShippingDelivery.shipping.through)
def signal_function(sender, instance, action, **kwargs):
	if action == "post_add":
		shippings = instance.shipping.all()
		for shipping in shippings:
			ShippingDeliveryStatus.set_in_delivery_shipping_status(shipping, instance)

class ShippingDeliveryStatus(models.Model):

	def __str__(self):
		return self.status.get_code_with_name() + " " + str(self.shipping)

	class Meta:
		verbose_name = 'Status Przesyłki'
		verbose_name_plural = '2. Statusy Przesyłek'

	status 		= models.ForeignKey(ShippingStatus, on_delete=models.CASCADE, verbose_name="Status")
	shipping 	= models.ForeignKey(Shipping, related_name='shipping_delivery_status', on_delete=models.CASCADE, verbose_name="Przesyłka")
	delivery 	= models.ForeignKey(ShippingDelivery, blank=True, null=True, on_delete=models.CASCADE, verbose_name="Dostawa")
	sign 		= models.ImageField(upload_to='signs/%Y/%m/%d/', max_length=255, blank=True, null=True, verbose_name="Podpis")
	created 	= models.DateTimeField(auto_now_add=True, verbose_name="Utworzono")

	def initialize_shipping_status(instance):
		status = ShippingStatus.get_initialize_status()
		ShippingDeliveryStatus.objects.create(status=status, shipping=instance)

	def set_in_delivery_shipping_status(shipping, delivery):
		status = ShippingStatus.get_in_delivery_status()
		ShippingDeliveryStatus.objects.create(status=status, shipping=shipping, delivery=delivery)

		
	def sign_thumb(self):
		return mark_safe("<img src='{0}'/>".format(self.sign.url))
	sign_thumb.allow_tags = True
	sign_thumb.short_description = "Podpis"

	def get_shipping_code(self):
		return self.shipping.code
	get_shipping_code.short_description = "Kod przesyłki"

	def get_created(self):
		return self.created.strftime("%d.%m.%Y %H:%M")
	get_created.short_description = "Utworzono"


@receiver(pre_save, sender=ShippingDeliveryStatus)
def pre_save_shipping_delivery_callback(sender, instance, *args, **kwargs):
	try:
		instance.sign.file
	except:
		pass
	finally:
		name = string_generator()
		try:
			_, b64_file = str(instance.sign).split(',')
			image_data = b64decode(b64_file)
			instance.sign =  ContentFile(image_data, "{0}-{1}.png".format(datetime.now().strftime("%Y%m%d%H%M%S"), name))
		except:
			pass
			# log error

@receiver(post_save, sender=ShippingDeliveryStatus)
def post_save_shipping_delivery_callback(sender, instance, created, **kwargs):
	if created:
		ShippingDeliveryLocalization.initialize_shipping_loc(instance)


class CompanyDepartment(models.Model):
	def __str__(self):
		return self.name

	class Meta:
		verbose_name = 'Oddział'
		verbose_name_plural = '6. Oddziały'

	name 	= models.CharField(max_length=64, blank=False, null=False, verbose_name="Nazwa",)
	adress	= models.CharField(max_length=64, blank=False, null=False, verbose_name="Adres",)
	lon 	= models.FloatField(blank=True, null=True, verbose_name="Koord. (lon)")
	lat		= models.FloatField(blank=True, null=True, verbose_name="Koord. (lat)")

	def get_default_shipping_send_place():
		return CompanyDepartment.objects.first()

@receiver(pre_save, sender=CompanyDepartment)
def pre_save_shipping_delivery_coords_callback(sender, instance, *args, **kwargs):
	if not instance.lon:
		try:
			geolocator = Nominatim()
			location = geolocator.geocode(instance.adress)
			instance.lon = location.latitude
			instance.lat = location.longitude
		except:
			pass
			#log error


class ShippingDeliveryLocalization(models.Model):
	def __str__(self):
		return str(self.delivery_status) + " (" + self.department.name + " "+self.get_created() + ")"

	class Meta:
		verbose_name = 'Lokalizacja przesyłki'
		verbose_name_plural = '3. Lokalizacje przesyłek'

	delivery_status	= models.ForeignKey(ShippingDeliveryStatus, related_name="shipping_delivery_locatization", on_delete=models.CASCADE, verbose_name="Status dostawy")
	department 		= models.ForeignKey(CompanyDepartment, on_delete=models.CASCADE, verbose_name="Lokalizacja")
	created 		= models.DateTimeField(auto_now_add=True, verbose_name="Utworzono")

	def initialize_shipping_loc(deliver_status):
		department = CompanyDepartment.get_default_shipping_send_place()
		deliver_status = ShippingDeliveryStatus.objects.filter(id=deliver_status.id).first()
		ShippingDeliveryLocalization.objects.create(delivery_status=deliver_status, department=department)

	def get_shipping_code(self):
		return self.delivery_status.shipping.code
	get_shipping_code.short_description = "Kod przesyłki"

	def get_shipping_status(self):
		return self.delivery_status.status.name
	get_shipping_status.short_description = "Status"

	def get_created(self):
		return self.created.strftime("%d.%m.%Y %H:%M")
	get_created.short_description = "Utworzono"