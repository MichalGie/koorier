import re
from django.core.exceptions import ValidationError

def phone_validator(value):
	rule = re.compile(r'(\d{3}[-\.\s]??\d{3}[-\.\s]??\d{4}|\(\d{3}\)\s*\d{3}[-\.\s]??\d{4}|\d{3}[-\.\s]??\d{4})')

	if not rule.search(value):
		raise ValidationError('Phone number not correct!')

