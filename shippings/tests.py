import base64
import json
import os
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.core.files import File
from rest_framework import status
from rest_framework_jwt.settings import api_settings
from rest_framework.reverse import reverse
from rest_framework.renderers import JSONRenderer
from rest_framework.test import APITestCase
from rest_framework.test import APIClient, APIRequestFactory, force_authenticate
from shippings.api.serializers import ShippingStatusSerializer
from shippings.forms import ShippingForm, CheckShippingForm
from shippings.models import CompanyDepartment, Shipping, ShippingStatus, ShippingDelivery, CourierDeliveryLog, ShippingDeliveryStatus

payload_handler = api_settings.JWT_PAYLOAD_HANDLER
encode_handler  = api_settings.JWT_ENCODE_HANDLER

User = get_user_model()

class ShipppingsAPITestCase(APITestCase):
	def setUp(self):	
		user = User(username='michal', email='email@aa.pl')
		user.set_password('haslo123')
		user.save()

		group = Group.objects.create(name="Kurierzy")
		group.user_set.add(user);

		shipping_status = ShippingStatus.objects.create(code="n01", name="wysłano", description="wyslano paczke", auth_group=group)
		shipping_status = ShippingStatus.objects.create(code="l03", name="do doreczenia", description="paczke do doreczenia", auth_group=group)
		shipping_status_2 = ShippingStatus.objects.create(code="w01", name="odebrano", description="odebrano paczke", auth_group=group)
		shipping_status_3 = ShippingStatus.objects.create(code="k01", name="odebrano", description="odebrano paczke", auth_group=group)
		department_1 = CompanyDepartment.objects.create(name="Dep 01", adress="Aleje Jerozolimskie 54 00-024 Warszawa")

		shipping = Shipping.objects.create(code="55875545458", width=12, height=34, weight=52, depth=15,
											sender_fullname="Michał",sender_phone="5548555", sender_email="aaa@aaa.pl",
											sender_adress="Kielcecka 78", sender_city="Kielce", sender_post_code="44-788",
											recipient_fullname="Kacper", recipient_phone="8854488", recipient_email="bbb@bbb.pl",
											recipient_adress="Warszawska 44", recipient_city="Warszawa", recipient_post_code="77-888",
										)

		shipping_delivery = ShippingDelivery.objects.create(courier=user)
		shipping_delivery.shipping.add(shipping)

		shipping_delivery_status = ShippingDeliveryStatus.objects.create(status=shipping_status, shipping=shipping, delivery=shipping_delivery)

	def test_shippings_count(self):
		count = Shipping.objects.all().count()
		self.assertEqual(count, 1)

	def test_shipping_status_count(self):
		count = ShippingStatus.objects.all().count()
		self.assertEqual(count, 3)

	def test_shipping_devivery_count(self):
		count = ShippingDelivery.objects.all().count()
		self.assertEqual(count, 1)

	def test_shipping_devivery_status_count(self):
		count = ShippingDeliveryStatus.objects.all().count()
		self.assertEqual(count, 3)

	def test_shipping_status_count(self):
		shipping = Shipping.objects.first()
		status = ShippingDeliveryStatus.objects.filter(shipping=shipping)
		self.assertEqual(status.count(), 3)

	def test_user_login(self):
		data = {}
		url = reverse('api-login')
		response = self.client.get(url, data, format='json')
		self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
		data = {
			'username' : "michal",
			'password' : "haslo1234",
		}
		response = self.client.post(url, data, format='json')
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
		data = {
			'username' : "michal",
			'password' : "haslo123",
		}
		response = self.client.post(url, data, format='json')
		self.assertEqual(response.status_code, status.HTTP_200_OK)

	def test_shipping_list_no_login(self):
		data = {}
		url = reverse('api-shippings:shipping-list')
		response = self.client.get(url, data, format='json')
		self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

	def test_shipping_list_login(self):
		user = User.objects.first()
		payload = payload_handler(user)
		encode = encode_handler(payload)
		self.client.credentials(HTTP_AUTHORIZATION="JWT "+encode)
		data = {}
		url = reverse('api-shippings:shipping-list')
		response = self.client.get(url, data, format='json')
		self.assertEqual(response.status_code, status.HTTP_200_OK)

	def test_shipping_status_list_create_no_login(self):
		shipping = Shipping.objects.first()
		data = {}
		url = reverse('api-shippings:shipping-status-create', kwargs={'code':shipping.code,})
		response = self.client.get(url, data, format='json',)
		self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

	def test_shipping_status_list_create_login(self):
		shipping = Shipping.objects.first()
		user = User.objects.first()
		payload = payload_handler(user)
		encode = encode_handler(payload)
		self.client.credentials(HTTP_AUTHORIZATION="JWT "+encode)
		data = {}
		url = reverse('api-shippings:shipping-status-create', kwargs={'code':shipping.code,})
		response = self.client.get(url, data, format='json')
		self.assertEqual(response.status_code, status.HTTP_200_OK)

	def test_shipping_status_create_no_shpiping_no_login(self):
		shipping = Shipping.objects.first()
		shipping_status = ShippingStatus.objects.first()
		data = {
			'status' : shipping_status.pk,
		}
		url = reverse('api-shippings:shipping-status-create', kwargs={'code':shipping.code,})
		response = self.client.post(url, data, format='json')
		self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

	def test_shipping_status_create_no_shipping_registered(self):
		shipping_status = ShippingStatus.objects.first()
		data = {
			'status_code' : 'w01',
		}
		user = User.objects.first()
		payload = payload_handler(user)
		encode = encode_handler(payload)
		self.client.credentials(HTTP_AUTHORIZATION="JWT "+encode)
		url = reverse('api-shippings:shipping-status-create', kwargs={'code':88454548,})
		response = self.client.post(url, data, format='json')
		self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

	def test_shipping_status_create_no_login(self):
		shipping = Shipping.objects.first()
		shipping_status = ShippingStatus.objects.first()
		data = {
			'status_code' : 'w01',
		}
		url = reverse('api-shippings:shipping-status-create', kwargs={'code':shipping.code,})
		response = self.client.post(url, json.dumps(data), format='json')
		self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

	def test_shipping_status_create_login_wrong_status_code(self):
		shipping = Shipping.objects.first()
		data = {
			'status_code' : 'z01',
		}
		user = User.objects.first()
		payload = payload_handler(user)
		encode = encode_handler(payload)
		self.client.credentials(HTTP_AUTHORIZATION="JWT "+encode)
		url = reverse('api-shippings:shipping-status-create', kwargs={'code':shipping.code,})
		response = self.client.post(url, data, format='json')
		self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

	def test_shipping_status_create_login_correct_status_code(self):
		shipping = Shipping.objects.first()
		data = {
			'status_code' : 'w01',
		}
		user = User.objects.first()
		payload = payload_handler(user)
		encode = encode_handler(payload)
		self.client.credentials(HTTP_AUTHORIZATION="JWT "+encode)
		url = reverse('api-shippings:shipping-status-create', kwargs={'code':shipping.code,})
		response = self.client.post(url, data, format='json')
		self.assertEqual(response.status_code, status.HTTP_201_CREATED)
	


	def test_shipping_status_create_login_correct_status_code_with_sign(self):
		shipping = Shipping.objects.first()
		user = User.objects.first()
		payload = payload_handler(user)
		encode = encode_handler(payload)
		self.client.credentials(HTTP_AUTHORIZATION="JWT "+encode)
		f = open(os.path.join(settings.MEDIA_ROOT, 'test/test.png'), 'rb')
		image = File(f)
		img = base64.b64encode(image.read())
		f.close()
		img = "data:image/png;base64," + img.decode()
		data = {
			'status_code' : 'w01',
		}
		url = reverse('api-shippings:shipping-status-create', kwargs={'code':shipping.code,})
		shipping_delivery_status = ShippingDeliveryStatus.objects.filter(shipping=shipping)
		self.assertEqual(shipping_delivery_status.count(), 3)
		response = self.client.post(url, data, format='json')
		self.assertEqual(response.status_code, status.HTTP_201_CREATED)
		shipping_delivery_status = ShippingDeliveryStatus.objects.filter(shipping=shipping)
		self.assertEqual(shipping_delivery_status.count(), 4)
		shipping_delivery_status = shipping_delivery_status.last()
		self.assertEqual(shipping.delivered, False)

	def test_shipping_status_create_login_correct_delivered_status_code_without_sign(self):
		shipping = Shipping.objects.first()
		data = {
			'status_code' : 'k05',
		}
		user = User.objects.first()
		payload = payload_handler(user)
		encode = encode_handler(payload)
		self.client.credentials(HTTP_AUTHORIZATION="JWT "+encode)
		url = reverse('api-shippings:shipping-status-create', kwargs={'code':shipping.code,})
		response = self.client.post(url, data, format='json')
		self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


	def test_shipping_status_create_login_correct_delivered_status_code_without_sign(self):
		shipping = Shipping.objects.first()
		user = User.objects.first()
		payload = payload_handler(user)
		encode = encode_handler(payload)
		self.client.credentials(HTTP_AUTHORIZATION="JWT "+encode)
		f = open(os.path.join(settings.MEDIA_ROOT, 'test/test.png'), 'rb')
		image = File(f)
		img = base64.b64encode(image.read())
		f.close()
		img = "data:image/png;base64," + img.decode()
		data = {
			'status_code' : 'k01',
			# 'sign_image': img,
		}
		url = reverse('api-shippings:shipping-status-create', kwargs={'code':shipping.code,})
		shipping_delivery_status = ShippingDeliveryStatus.objects.filter(shipping=shipping)
		self.assertEqual(shipping_delivery_status.count(), 3)
		response = self.client.post(url, data, format='json')
		self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
		shipping_delivery_status = ShippingDeliveryStatus.objects.filter(shipping=shipping)
		self.assertEqual(shipping_delivery_status.count(), 3)
		shipping_delivery_status = shipping_delivery_status.last()
		shipping = Shipping.objects.get(code=shipping.code)
		self.assertEqual(shipping.delivered, False)


	def test_shipping_status_create_login_correct_delivered_status_code_with_sign(self):
		shipping = Shipping.objects.first()
		user = User.objects.first()
		payload = payload_handler(user)
		encode = encode_handler(payload)
		self.client.credentials(HTTP_AUTHORIZATION="JWT "+encode)
		f = open(os.path.join(settings.MEDIA_ROOT, 'test/test.png'), 'rb')
		image = File(f)
		img = base64.b64encode(image.read())
		f.close()
		img = "data:image/png;base64," + img.decode()
		data = {
			'status_code' : 'k01',
			'sign_image': img,
		}
		url = reverse('api-shippings:shipping-status-create', kwargs={'code':shipping.code,})
		shipping_delivery_status = ShippingDeliveryStatus.objects.filter(shipping=shipping)
		self.assertEqual(shipping_delivery_status.count(), 3)
		response = self.client.post(url, data, format='json')
		self.assertEqual(response.status_code, status.HTTP_201_CREATED)
		shipping_delivery_status = ShippingDeliveryStatus.objects.filter(shipping=shipping)
		self.assertEqual(shipping_delivery_status.count(), 4)
		shipping_delivery_status = shipping_delivery_status.last()
		shipping = Shipping.objects.get(code=shipping.code)
		self.assertEqual(shipping.delivered, True)

	def test_courier_log_create_no_login(self):
		data = {
			'lon' :888.855,
			'lat' :878.84,
		}
		url = reverse('api-shippings:courier-log-create')
		response = self.client.post(url, data, format='json')
		self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

	def test_courier_log_create_login(self):
		data = {
			'lon' :888.855,
			'lat' :878.84,
		}
		user = User.objects.first()
		payload = payload_handler(user)
		encode = encode_handler(payload)
		self.client.credentials(HTTP_AUTHORIZATION="JWT "+encode)
		data = {}
		url = reverse('api-shippings:courier-log-create')
		response = self.client.post(url, data, format='json')
		self.assertEqual(response.status_code, status.HTTP_201_CREATED)

	def test_shipping_status_list_no_login(self):
		data = {}
		url = reverse('api-shippings:shipping-status-list')
		response = self.client.get(url, data, format='json',)
		self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

	def test_shipping_status_list_login(self):
		user = User.objects.first()
		payload = payload_handler(user)
		encode = encode_handler(payload)
		self.client.credentials(HTTP_AUTHORIZATION="JWT "+encode)
		data = {}
		url = reverse('api-shippings:shipping-status-list')
		response = self.client.get(url, data, format='json')
		self.assertEqual(response.status_code, status.HTTP_200_OK)


	def test_guest_shipping_status_no_login_wrong_code(self):
		data = {}
		url = reverse('api-shippings:shipping-status-guest', kwargs={'code':1234,})
		response = self.client.get(url, data, format='json')
		self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

	def test_guest_shipping_status_no_correct_code(self):
		shipping = Shipping.objects.first()
		data = {}
		url = reverse('api-shippings:shipping-status-guest', kwargs={'code':shipping.code,})
		response = self.client.get(url, data, format='json')
		self.assertEqual(response.status_code, status.HTTP_200_OK)


	def test_guest_courier_status_no_login_wrong_code(self):
		data = {}
		url = reverse('api-shippings:courier-status-guest', kwargs={'code':1234,})
		response = self.client.get(url, data, format='json')
		self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


	def test_guest_courier_status_no_login_correct_code_not_status(self):
		shipping2 = Shipping.objects.create(code="55811545458", width=12, height=34, weight=52, depth=15,
					sender_fullname="Michał",sender_phone="5548555", sender_email="aaa@aaa.pl",
					sender_adress="Kielcecka 78", sender_city="Kielce", sender_post_code="44-788",
					recipient_fullname="Kacper", recipient_phone="8854488", recipient_email="bbb@bbb.pl",
					recipient_adress="Warszawska 44", recipient_city="Warszawa", recipient_post_code="77-888",
				)
		data = {}
		url = reverse('api-shippings:courier-status-guest', kwargs={'code':shipping2.code,})
		response = self.client.get(url, data, format='json')
		self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
		shipping_delivery = ShippingDelivery.objects.first()
		shipping_delivery.shipping.add(shipping2)
		self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


	def test_guest_courier_status_no_correct_code(self):
		shipping = Shipping.objects.first()
		data = {}
		url = reverse('api-shippings:courier-status-guest', kwargs={'code':131231,})
		response = self.client.get(url, data, format='json')
		self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class ShipppingsFormsTestCase(APITestCase):

	def test_search_form_string(self):
		form_data = {'code': 'abc'}
		form = CheckShippingForm(data=form_data)
		self.assertFalse(form.is_valid())

	def test_search_form_to_short_number(self):
		form_data = {'code': 1234}
		form = CheckShippingForm(data=form_data)
		self.assertFalse(form.is_valid())

	def test_search_form_correct_number(self):
		form_data = {'code': 1234567890}
		form = CheckShippingForm(data=form_data)
		self.assertTrue(form.is_valid())

	def test_send_form_wrong_data(self):
		form_data = {}
		form = ShippingForm(data=form_data)
		self.assertFalse(form.is_valid())

		form_data = {
				'width' : 'abc',
				'height' : 'def',
				'weight' : 'acd',
				'depth' : 'aa',
				'sender_fullname' : 'razek dwasz',
				'sender_phone' : '777777',
				'sender_email' : 'mg@aaa.pl',
				'sender_adress' : 'polsk',
				'sender_city' : 'polska',
				'sender_post_code' : '88-999',
				'recipient_fullname' : 'michal kiel',
				'recipient_phone' : '7788558',
				'recipient_email' : 'raz@aaa.pl',
				'recipient_adress' : 'niemcy',
				'recipient_city' : 'niemc',
				'recipient_post_code' : '88-999',
			}
		form = ShippingForm(data=form_data)
		self.assertFalse(form.is_valid())

		form_data = {
				'width' : 77,
				'height' : 88,
				'weight' : 4,
				'depth' : 2,
				'sender_fullname' : 'Michal Dwasz',
				'sender_phone' : '777777',
				'sender_email' : 'mg@aaa.pl',
				'sender_adress' : 'polsk',
				'sender_city' : 'polska',
				'sender_post_code' : '88-999',
				'recipient_fullname' : 'michal kiel',
				'recipient_phone' : '7788558',
				'recipient_email' : 'raz@aaa.pl',
				'recipient_adress' : 'niemcy',
				'recipient_city' : 'niemc',
				'recipient_post_code' : '88-999',
			}
		form = ShippingForm(data=form_data)
		self.assertFalse(form.is_valid())


		form_data = {
				'width' : 77,
				'height' : 88,
				'weight' : 4,
				'depth' : 2,
				'sender_fullname' : 'Michal Dwasz',
				'sender_phone' : 89988444,
				'sender_email' : 'mg@aaa.pl',
				'sender_adress' : 'polsk',
				'sender_city' : 'polska',
				'sender_post_code' : '88-999',
				'recipient_fullname' : 'michal kiel',
				'recipient_phone' : 8889944,
				'recipient_email' : 'raz@aaa.pl',
				'recipient_adress' : 'niemcy',
				'recipient_city' : 'niemc',
				'recipient_post_code' : '88-999',
			}
		form = ShippingForm(data=form_data)
		self.assertTrue(form.is_valid())