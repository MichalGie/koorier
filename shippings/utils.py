import random
import string

def generate_shiping_code(model):
	rand = 0
	while True:
		rand = random.randint(1000000000,999999999999)
		obj = model.objects.filter(code=rand)
		if not obj:
			break
	return str(rand)

def string_generator(size=10, chars=string.ascii_uppercase + string.ascii_lowercase + string.digits):
	return ''.join(random.choice(chars) for _ in range(size))