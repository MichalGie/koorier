from django.contrib import admin
from shippings.models import CourierDeliveryLog, CompanyDepartment, Shipping, ShippingDelivery, ShippingDeliveryStatus, ShippingDeliveryLocalization, ShippingStatus
from shippings.forms import ShippingDeliveryAdminForm

class CourierDeliveryLogAdmin(admin.ModelAdmin):
	model = CourierDeliveryLog
	fields = ("courier", "lon", "lat", "created",)
	readonly_fields = ("lon", "lat", "created", )
	list_display = ("get_courier_full_name", "lon", "lat", "get_created",)
admin.site.register(CourierDeliveryLog, CourierDeliveryLogAdmin)

 
class ShippingStatusAdmin(admin.ModelAdmin):
	model = ShippingStatus
	fields = ("code", "name", "description", "auth_group",)
	list_display = ("code", "name", "auth_group",)
admin.site.register(ShippingStatus, ShippingStatusAdmin)


class ShippingDeliveryAdmin(admin.ModelAdmin):
	form = ShippingDeliveryAdminForm
	filter_horizontal = ('shipping',)
	list_display = ("get_created", "courier", "created", )
	readonly_fields = ("created", )
admin.site.register(ShippingDelivery, ShippingDeliveryAdmin)


class ShippingAdmin(admin.ModelAdmin):
	model = Shipping
	fields = ("code", "width", "height", "weight", "depth", "sender_fullname", "sender_phone", "sender_email", "sender_adress", 
				"sender_city", "sender_post_code", "recipient_fullname", "recipient_phone", "recipient_email", "recipient_adress",
				"recipient_city","recipient_post_code", "recipient_lon", "recipient_lat", "updated", "created", "delivered", 
			)
	readonly_fields = ("code", "recipient_lon", "recipient_lat", "updated", "created", )

	list_display = ("code", "recipient_fullname", "recipient_city", "sender_fullname", "delivered", "get_updated", "get_created",)
admin.site.register(Shipping, ShippingAdmin)


class ShippingDeliveryStatusAdmin(admin.ModelAdmin):
	model = ShippingDeliveryStatus
	fields = ("status", "shipping", "delivery", "sign" , "sign_thumb", "created", )
	readonly_fields = ("sign_thumb", "created")
	list_display = ("get_shipping_code", "status", "delivery", "get_created",)
admin.site.register(ShippingDeliveryStatus, ShippingDeliveryStatusAdmin)


class ShippingDeliveryLocalizationAdmin(admin.ModelAdmin):
	model = ShippingDeliveryLocalization
	fields = ("delivery_status", "department", "created",)
	readonly_fields = ("created",)
	list_display = ("get_shipping_code", "get_shipping_status", "department", "get_created")
admin.site.register(ShippingDeliveryLocalization, ShippingDeliveryLocalizationAdmin)


class CompanyDepartmentAdmin(admin.ModelAdmin):
	model = CompanyDepartment
	fields = ("name", "adress", "lon", "lat",)
	readonly_fields = ("lon", "lat",)
	list_display = ("name", "adress", )
admin.site.register(CompanyDepartment, CompanyDepartmentAdmin)