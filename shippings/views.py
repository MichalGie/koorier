from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.template import Context
from django.template.loader import get_template
from django.urls import reverse
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView, FormView
from django.views.generic.list import ListView
from shippings.forms import CheckShippingForm, ShippingForm
from shippings.models import CourierDeliveryLog, Shipping, ShippingDeliveryLocalization, ShippingDeliveryStatus
from shippings.messages import messages as messages_text
from math import sin, cos, sqrt, atan2, radians


class HomeView(TemplateView):
	template_name = "shippings/home.html"


class ContactView(TemplateView):
	template_name = "shippings/contact.html"


class CheckShippingFormView(FormView):
	template_name = "shippings/check_shipping.html";
	form_class = CheckShippingForm


	def form_valid(self, form):
		code = form.cleaned_data['code']
		url = reverse('shippings-views:check-shipping-status', kwargs={'code': code})
		return HttpResponseRedirect(url)


class SendShippingCreateView(CreateView):
	model = Shipping
	form_class = ShippingForm
	template_name = 'shippings/send_shipping.html'
	template_name_success = 'shippings/send_shipping_success.html'
	success_url = '/'

	def form_valid(self, form):
		self.object = form.save()
		self.context = {
			'code' 				: self.object.code,
			'code_link' 		: self.request.build_absolute_uri(reverse('views-shippings:check-shipping-status', args=(self.object.code, ))),
			'sender_name' 		: self.object.sender_fullname,
			'sender_email'		: self.object.sender_email,
			'recipient_name' 	: self.object.recipient_fullname,
			'recipient_email'	: self.object.recipient_email,
		}

		plaintext = get_template('emails/new_shipping.txt')
		htmly     = get_template('emails/new_shipping.html')

		subject = 'Nadana została nowa przesyłka'
		text_content = plaintext.render(self.context)
		html_content = htmly.render(self.context)
		for recipient in [self.object.sender_email, self.object.recipient_email]:
			msg = EmailMultiAlternatives(subject, text_content, settings.MY_EMAIL, [recipient])
			msg.attach_alternative(html_content, "text/html")
			msg.send()
		return render(self.request, self.template_name_success, self.context)


class CheckShippingStatusListView(ListView):
	model = ShippingDeliveryStatus
	template_name = 'shippings/track_shipping.html'

	def dispatch(self, request, *args, **kwargs):
		code = self.kwargs.get('code')
		query = ShippingDeliveryStatus.objects.filter(shipping__code=code)
		if query:
			return super().dispatch(request, *args, **kwargs)
		messages.add_message(request, messages.ERROR, messages_text['NO-PACKAGE-ERROR'])
		url = reverse('shippings-views:check-shipping')
		return HttpResponseRedirect(url)

	def get_queryset(self):
		code = self.kwargs.get('code')
		query = ShippingDeliveryStatus.objects.filter(shipping__code=code).order_by('id')
		return query	

	def calculate_zoom(self, lat1, lon1, lat2, lon2):
		# approximate radius of earth in km
		R = 6373.0

		lat1 = radians(lat1)
		lon1 = radians(lon1)
		lat2 = radians(lat2)
		lon2 = radians(lon2)

		dlon = lon2 - lon1
		dlat = lat2 - lat1

		a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
		c = 2 * atan2(sqrt(a), sqrt(1 - a))

		distance = R * c

		zoom = 4
		if distance < 20:
			zoom = 12
		if 20 < distance < 50:
			zoom = 9
		elif 50 < distance < 150:
			zoom = 7
		elif 150< distance < 600:
			zoom = 5

		return zoom

	def get_context_data(self, **kwargs):
		code = self.kwargs.get('code')
		context = super().get_context_data(**kwargs)
		query = super().get_queryset()
		shipping = Shipping.objects.filter(code=code).first()
		courier_delivery = None
		if query.first().delivery:
			courier = query.first().delivery.courier
			courier_delivery = CourierDeliveryLog.objects.filter(courier=courier).last()
		shipping_delivery_loc = ShippingDeliveryLocalization.objects.filter(delivery_status__shipping=shipping).last()

		if shipping:
			context['lat'] = shipping.recipient_lat
			context['lon'] = shipping.recipient_lon
			if shipping_delivery_loc:
				if courier_delivery is None:
					context['last_status_lat'] = shipping_delivery_loc.department.lat
					context['last_status_lon'] = shipping_delivery_loc.department.lon
				else:
					if courier_delivery.created > shipping_delivery_loc.created:
						context['last_status_lat'] = courier_delivery.lat
						context['last_status_lon'] = courier_delivery.lon
					else:
						context['last_status_lat'] = shipping_delivery_loc.department.lat
						context['last_status_lon'] = shipping_delivery_loc.department.lon
			context['zoom'] = self.calculate_zoom(context['last_status_lat'], context['last_status_lon'], context['lat'], context['lon'])
		return context