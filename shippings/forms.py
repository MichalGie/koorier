from django import forms
from django.core import validators
from shippings.models import Shipping, ShippingDelivery

class ShippingForm(forms.ModelForm):
	class Meta:
		model = Shipping
		fields = ["width", "height", "weight", "depth", "sender_fullname","sender_phone", "sender_email", "sender_adress", "sender_city", "sender_post_code",
					"recipient_fullname", "recipient_phone", "recipient_email", "recipient_adress", "recipient_city", "recipient_post_code"]

		widgets = { 
			'width': forms.NumberInput(attrs={'placeholder': u'16(cm)', "name": u"width"},),
			'height': forms.NumberInput(attrs={'placeholder': u'32(cm)'},),
			'weight': forms.NumberInput(attrs={'placeholder': u'4(kg)'},),
			'depth': forms.NumberInput(attrs={'placeholder': u'8(cm)'},),
			'sender_fullname': forms.TextInput(attrs={'placeholder': u'Jan Kowalski'}),
			'sender_phone': forms.TextInput(attrs={'placeholder': u'888 444 888'},),
			'sender_email': forms.EmailInput(attrs={'placeholder': u'jan.kowalski@email.com'},),
			'sender_adress': forms.TextInput(attrs={'placeholder': u'Niska 26'},),
			'sender_city': forms.TextInput(attrs={'placeholder': u'Warszawa'},),
			'sender_post_code': forms.TextInput(attrs={'placeholder': u'201-046'},),
			'recipient_fullname': forms.TextInput(attrs={'placeholder': u'Jan Marek'},),
			'recipient_phone': forms.TextInput(attrs={'placeholder': u'888 222 444'},),
			'recipient_email': forms.EmailInput(attrs={'placeholder': u'jan.marek@email.com'},),
			'recipient_adress': forms.TextInput(attrs={'placeholder': u'11 Listopada 62'},),
			'recipient_city': forms.TextInput(attrs={'placeholder': u'Radom'},),
			'recipient_post_code': forms.TextInput(attrs={'placeholder': u'26-615'},),


		}


class CheckShippingForm(forms.Form):
	code = forms.IntegerField(validators=[validators.MinValueValidator(1000000000, message="Shipping number require to have 10 characters!"),])

	
class ShippingDeliveryAdminForm(forms.ModelForm):
	class Meta:
		model = ShippingDelivery
		fields = ("shipping","courier",)

	def __init__(self, *args, **kwargs):
		super(ShippingDeliveryAdminForm, self).__init__(*args, **kwargs)
		self.fields['shipping'].queryset = Shipping.objects.filter(delivered=False)