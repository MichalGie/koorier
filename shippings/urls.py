from django.urls import path
from shippings.views import ContactView, CheckShippingFormView, HomeView, SendShippingCreateView, CheckShippingStatusListView

app_name = 'shippings-views'
urlpatterns = [
   path('send/', SendShippingCreateView.as_view(), name='send-shipping'),
   path('check-status/', CheckShippingFormView.as_view(), name='check-shipping'),
   path('contact/', ContactView.as_view(), name='contact'),
   path('check-status/<int:code>', CheckShippingStatusListView.as_view(), name='check-shipping-status'),
   path('contact/', ContactView.as_view(), name='contact'),
   path('', HomeView.as_view(), name='home'),
]