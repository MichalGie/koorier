from django.db.models import Q
from rest_framework import generics
from rest_framework.permissions import AllowAny
from django.http import Http404
from shippings.models import CourierDeliveryLog, Shipping, ShippingDeliveryStatus, ShippingStatus
from shippings.api.serializers import ShippingSerializer, CourierDeliveryLogSerializer, ShippingDeliveryStatusSerializer, ShippingStatusSerializer, ShippingDeliveryStatusGuestSerializer, CourierDeliveryLogGuestSerializer

class ShippingDeliveryStatusCreateAPIView(generics.ListCreateAPIView):
	serializer_class = ShippingDeliveryStatusSerializer

	def get_queryset(self):
		code = self.kwargs['code']
		user = self.request.user
		if code:
			obj = ShippingDeliveryStatus.objects.filter(shipping__code=code, delivery__courier=user, shipping__delivered=False)
			if obj:
				return obj
		raise Http404()

	def perform_create(self, serializer):
		if self.get_queryset():
			code = self.kwargs['code']
			user = self.request.user
			obj = ShippingDeliveryStatus.objects.filter(shipping__code=code, delivery__courier=user).first()
			if serializer.is_valid():
				status_code = serializer.validated_data.get("status")
				status = ShippingStatus.objects.filter(code=status_code.get('code')).first()
				if not status:
					raise Http404
				shipping = obj.shipping
				if status.code in ("k01","k05"):
					sign = serializer.validated_data.get("sign")
					if sign:
						delivered = True
						shipping.delivered = delivered
						shipping.save()
					else:
						raise Http404()
				serializer.save(delivery=obj.delivery, shipping=shipping, status=status)

class CourierDeliveryLogCreateAPIView(generics.ListCreateAPIView):
	serializer_class = CourierDeliveryLogSerializer

	def get_queryset(self):
		user = self.request.user
		obj = CourierDeliveryLog.objects.filter(courier=user)
		return obj

	def perform_create(self, serializer):
		serializer.save(courier=self.request.user)

class ShippingListAPIView(generics.ListAPIView):
	serializer_class = ShippingSerializer

	def get_queryset(self):
		user = self.request.user
		obj = Shipping.objects.filter(shipping_delivery__courier=user, delivered=False)
		return obj

class ShippingStatusListAPIView(generics.ListAPIView):
	serializer_class = ShippingStatusSerializer

	def get_queryset(self):
		obj = ShippingStatus.objects.filter(auth_group__name="Kurierzy")
		return obj

class ShippingStatusCheckRetriveAPIView(generics.ListAPIView):
	serializer_class = ShippingDeliveryStatusGuestSerializer

	permission_classes = [
		AllowAny,
	]

	def get_queryset(self):
		code = self.kwargs['code']
		obj = ShippingDeliveryStatus.objects.filter(shipping__code=code)
		if not obj:
			raise Http404()
		return obj


class CourierDeliveryByShippingListAPIView(generics.ListAPIView):
	serializer_class = CourierDeliveryLogGuestSerializer

	permission_classes = [
		AllowAny,
	]

	def get_queryset(self):
		code = self.kwargs.get('code')
		shipping_delivery = ShippingDeliveryStatus.objects.filter(shipping__code=code)
		if not shipping_delivery:
			raise Http404()
		try:
			courier = shipping_delivery.first().delivery.courier
		except:
			raise Http404()
		courier_delivery = CourierDeliveryLog.objects.filter(courier=courier)
		return courier_delivery