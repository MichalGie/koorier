from base64 import b64decode, b64encode
from django.core.files import File
from rest_framework import serializers
from django.contrib.auth.models import User
from shippings.models import CourierDeliveryLog, Shipping, ShippingDelivery, ShippingDeliveryStatus, ShippingStatus

class ShippingSerializer(serializers.ModelSerializer):
	class Meta:
		model = Shipping
		fields = [
			'code',
			'recipient_fullname',
			'recipient_phone',
			'recipient_email',
			'recipient_adress',
			'recipient_city',
			'recipient_post_code',
		]
		read_only_fiels = []

class ShippingStatusSerializer(serializers.ModelSerializer):
	class Meta:
		model = ShippingStatus
		lookup_field = 'code'
		fields = (
			'code',
			'name',
		)
		read_only_fiels = []

class UserSerializer(serializers.ModelSerializer):
	class Meta:
		model = User
		fields = (
			'first_name',
			'second_name',
			'email',
			'updated',
			'created',
		)
		read_only_fiels = ['updated','created',]

class ShippingDeliverySerializer(serializers.ModelSerializer):
	shipping = ShippingSerializer()
	courier = UserSerializer()
	class Meta:
		model = ShippingDelivery
		fields = (
			'shipping',
			'courier',
		)

def validate_b64_image(value):
	try:
		_, b64_file = value.split(',')
		if b64encode(b64decode(b64_file)).decode('utf-8') != b64_file:
			raise serializers.ValidationError('Not valid b64 image!')
	except:
		raise serializers.ValidationError('Not valid b64 image!')


class ShippingDeliveryStatusSerializer(serializers.ModelSerializer):
	status_code = serializers.CharField(read_only=False, source='status.code')
	status_name = serializers.CharField(read_only=True, source='status.name')
	sign_image	= serializers.CharField(read_only=False, required=False, source='sign', validators=[validate_b64_image])

	class Meta:
		model = ShippingDeliveryStatus
		fields = (
			'status_code',
			'sign_image',
			'status_name',
			'created',
		)
		read_only_fiels = ['status_name','created',]


class ShippingDeliveryStatusGuestSerializer(serializers.ModelSerializer):
	status_code = serializers.CharField(read_only=False, source='status.code')
	status_name = serializers.CharField(read_only=True, source='status.name')
	delivery_lon = serializers.FloatField(read_only=True, source='shipping.recipient_lon')
	delivery_lat = serializers.FloatField(read_only=True, source='shipping.recipient_lat')

	class Meta:
		model = ShippingDeliveryStatus
		fields = (
			'status_code',
			'status_name',
			'delivery_lon',
			'delivery_lat',
			'created',
		)
		read_only_fiels = ['status_code', 'status_name','created',]


class CourierDeliveryLogSerializer(serializers.ModelSerializer):
	class Meta:
		model = CourierDeliveryLog
		fields = [
			'courier',
			'lon',
			'lat',
			'created',
		]
		read_only_fields = ['courier', 'created',]

class CourierDeliveryLogGuestSerializer(serializers.ModelSerializer):
	class Meta:
		model = CourierDeliveryLog
		fields = [
			'courier',
			'lon',
			'lat',
			'created',
		]
		read_only_fields = ['courier', 'lon', 'lat', 'created',]