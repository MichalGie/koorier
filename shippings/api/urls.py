from django.urls import path
from shippings.api.views import ShippingDeliveryStatusCreateAPIView, CourierDeliveryLogCreateAPIView, CourierDeliveryByShippingListAPIView, ShippingListAPIView, ShippingStatusCheckRetriveAPIView, ShippingStatusListAPIView


app_name = 'shippings'
urlpatterns = [
   path('status/<int:code>/', ShippingDeliveryStatusCreateAPIView.as_view(), name='shipping-status-create'),
   path('courier/', CourierDeliveryLogCreateAPIView.as_view(), name='courier-log-create'),
   path('guest-courier-status/<int:code>', CourierDeliveryByShippingListAPIView.as_view(), name='courier-status-guest'),
   path('guest-shipping-status/<int:code>', ShippingStatusCheckRetriveAPIView.as_view(), name='shipping-status-guest'),
   path('status/', ShippingStatusListAPIView.as_view(), name='shipping-status-list'),
   path('', ShippingListAPIView.as_view(), name='shipping-list'),
]