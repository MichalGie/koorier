# Generated by Django 2.0.4 on 2018-05-16 09:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shippings', '0015_auto_20180516_0941'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shipping',
            name='sign',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
