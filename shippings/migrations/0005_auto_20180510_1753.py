# Generated by Django 2.0.4 on 2018-05-10 17:53

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('shippings', '0004_shippingstatus_auth_group'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='shippingdeliverystatus',
            name='delivery',
        ),
        migrations.AddField(
            model_name='shippingdeliverystatus',
            name='shipping',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='shipping_delivery_status', to='shippings.Shipping'),
            preserve_default=False,
        ),
    ]
